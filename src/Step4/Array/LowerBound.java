package Step4.Array;

public class LowerBound {
    public static void main(String[] args) {
        int[] arr = {1, 2, 2, 3, 3, 5};
        int target = 7;
        System.out.println(lowerBound(arr, target));
    }

    private static int lowerBound(int[] arr, int target) {
        int currentIndex = -1;
        int left = 0;
        int right = arr.length - 1;
        int mid = left + (right - left) / 2;

        while(left < right) {
            if(arr[mid] >= target) {
                currentIndex = mid;
                right = mid - 1;
            } else if(arr[mid] < target) {
                left = mid + 1;
                currentIndex = mid + 1;
            }
            mid = left + (right - left) / 2;
        }

        return currentIndex;
    }
}
