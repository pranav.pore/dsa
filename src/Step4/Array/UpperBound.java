package Step4.Array;

public class UpperBound {
    public static void main(String[] args) {
        int[] arr = {1, 2, 2, 3};
        int target = 2;
        System.out.println(upperBound(arr, target));
    }

    private static int upperBound(int[] arr, int target) {
        int left = 0;
        int right = arr.length - 1;
        int mid = left + (right - left) / 2;
        int currentIndex = -1;
        while(left < right) {
            if(arr[mid] >= target) {
                currentIndex = mid;
                left = mid + 1;
            } else
                right = mid;
            mid = left + (right - left) / 2;
        }

        return currentIndex;
    }
}
