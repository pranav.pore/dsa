public class SelectionSort {
    public static void main(String[] args) {
        int[] arr = { 5, 2, 3, 1, 8 };
        selectionSort(arr);
        printArray(arr);
    }

    private static void printArray(int[] arr) {
        for(int i : arr)
            System.out.print(i + " ");
        System.out.println();
    }

    private static void selectionSort(int[] arr) {
        for(int i = 0; i < arr.length; i++) {
            int minIdx = i;
            for(int j = minIdx; j < arr.length; j++) 
                if(arr[j] < arr[minIdx]) minIdx = j;
            int temp = arr[i];
            arr[i] = arr[minIdx];
            arr[minIdx] = temp;
        }
    }
}