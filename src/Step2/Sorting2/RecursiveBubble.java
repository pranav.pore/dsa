package Step2.Sorting2;

public class RecursiveBubble {
    public static void main(String[] args) {
        int[] arr = { 5, 2, 3, 1, 8 };
        recursiveBubble(arr, arr.length);
        printArray(arr);
    }   

    private static void printArray(int[] arr) {
        for(int i : arr)
            System.out.print(i + " ");
        System.out.println();
    }

    private static void recursiveBubble(int[] arr, int n) {
        if (n == 1) return;

        for(int i = 0; i < n - 1; i++) {
            if (arr[i] > arr[i + 1]) {
                int temp = arr[i];
                arr[i] = arr[i + 1];
                arr[i + 1] = temp;
            }
        }
        recursiveBubble(arr, n - 1);
    } 
}
