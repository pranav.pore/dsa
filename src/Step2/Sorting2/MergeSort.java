package Step2.Sorting2;

import Step3.Hard.ReversePairs;

import java.util.*;

public class MergeSort {
    public static void main(String[] args) {
        int[] arr = { 5, 4, 3, 2, 1 };
        mergeSort(arr, 0, arr.length - 1);
        printArray(arr);
    }

    private static void printArray(int[] arr) {
        for(int i : arr)
            System.out.print(i + " ");
        System.out.println();
    }

    private static void mergeSort(int[] arr, int low, int high) {
        if(low == high) return;
        int mid = (low + high) / 2; 
        mergeSort(arr, low, mid);
        mergeSort(arr, mid + 1, high);
        merge(arr, low, mid, high);
    }

    private static void merge(int[] arr, int low, int mid, int high) {
        ArrayList<Integer> resultArr = new ArrayList<>();
        int i = low;
        int j = mid + 1;
        ReversePairs.insertElements(arr, low, mid, high, i, j, resultArr);
    }
}
