package Step2.Sorting2;

public class RecursiveInsertion {
    public static void main(String[] args) {
        int[] arr = { 5, 4, 3, 2, 1 };
        recursiveInsertion(arr, 1);
        printArray(arr);
    }

    private static void printArray(int[] arr) {
        for(int i : arr)
            System.out.print(i + " ");
        System.out.println();
    }
    
    private static void recursiveInsertion(int[] arr, int i) {
        if(i == arr.length) return;
        int j = i;
        while(j > 0 && arr[j - 1] > arr[j]) {
            int temp = arr[j - 1];
            arr[j - 1] = arr[j];
            arr[j] = temp;
            j--;
        }
        recursiveInsertion(arr, i + 1);
    }
}
