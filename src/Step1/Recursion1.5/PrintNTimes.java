public class PrintNTimes {
    public static void main(String[] args) {
        printNTimes(5, "Hello, World!");
    }   
    
    private static void printNTimes(int count, String string) {
        System.out.println(string);
        if(count < 2)
            return;
        printNTimes(count - 1, string);
    }
}
