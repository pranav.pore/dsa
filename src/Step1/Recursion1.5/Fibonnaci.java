public class Fibonnaci {
    public static void main(String[] args) {
        int n = 4;
        System.out.println(fibonnaci(n, 0, 0, 1));
    }
    
    private static int fibonnaci(int n, int i, int first, int second) {
        if(n == i - 1) return second;
        return fibonnaci(n, i + 1, second, first + second);
    }
}
