public class Print1ToN {
    public static void main(String[] args) {
        print1ToN(1, 5);
    }

    private static void print1ToN(int i, int number) {
        if(i > number) return;
        System.out.print(i + " ");
        print1ToN(i + 1, number);
    }
}
