public class ReverseArray {
    public static void main(String[] args) {
        int[] arr = {3, 1, 1, 7, 4, 2, 6, 11};
        printArray(arr);
        int[] reversedArray = reverseArray(0, arr);
        printArray(reversedArray);
    }

    private static void printArray(int[] arr) {
        for(int i : arr) 
            System.out.print(i + " ");
        System.out.println();
    }

    private static int[] reverseArray(int n, int[] nums) {
        if(n == nums.length / 2) 
            return nums;
        int temp = nums[n];
        nums[n] = nums[nums.length - (n + 1)];
        nums[nums.length - (n + 1)] = temp;
        return reverseArray(n + 1, nums);
    }
}
