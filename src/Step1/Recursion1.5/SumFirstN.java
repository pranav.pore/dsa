public class SumFirstN {
    public static void main(String[] args) {
        sumOfFirstN(10, 0);
    }

    private static void sumOfFirstN(int i, int sum) {
        if(i < 1) {
            System.out.println("Sum is: "  + sum);
            return;
        }
        sumOfFirstN(i - 1, sum + i);
    }
}
