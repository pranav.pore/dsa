public class Factorial {
    public static void main(String[] args) {
        calcFactorial(5, 1);
    }   
    
    private static void calcFactorial(int i, int factorial) {
        if(i < 2) {
            System.out.println("Factorial is: " + factorial);
            return;
        }
        calcFactorial(i - 1, factorial * i);
    }
}
