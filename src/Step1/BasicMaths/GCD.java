public class GCD {
    public static void main(String[] args) {
        int number1 = 54;
        int number2 = 9;
        System.out.println("GCD of " + number1 + " and " + number2 + " is: " + calculateGcd(number1, number2));
    }

    private static int calculateGcd(int number1, int number2) {
        int smaller = number1 < number2 ? number1 : number2;

        for(int i = smaller; i >= 2; i--) 
            if(number1 % i == 0 && number2 % i == 0) return i;
        return 1;
    }
}