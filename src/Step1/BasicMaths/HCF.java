public class HCF {
    public static void main(String[] args) {
        int number1 = 3;
        int number2 = 2;
        System.out.println("HCF of " + number1 + " and " + number2 + " is: " + calculateHcf(number1, number2));
    }   
    
    private static int calculateHcf(int number1, int number2) {
        int smaller = number1 < number2 ? number1 : number2;
        int gcd = 1;

        for(int i = smaller; i >= 2; i--) {
            if(number1 % i == 0 && number2 % i == 0) {
                gcd = i;
                break;
            }
        }

        return (number1 * number2) / gcd;
    }
}
