public class CountLetters {
    public static void main(String[] args) {
        String str = "abcdabefc";
        int[] countArr = countLetters(str);
        printArray(countArr);
    }    

    private static void printArray(int[] countArr) {
        for(int i = 0; i < countArr.length; i++) 
            System.out.println((char)('a' + i) + ": " + countArr[i]);
    }

    private static int[] countLetters(String str) {
        int[] countArr = new int[26];
        for(int i = 0; i < str.length(); i++) 
            countArr[str.charAt(i) - 'a']++;
        return countArr;
    }
}
