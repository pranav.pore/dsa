public class CountHash {
    public static void main(String[] args) {
        int[] arr = {1, 2, 1, 3, 2};
        int[] countArr = countHash(arr, 3);
        printArray(countArr);
    }    

    private static void printArray(int[] arr) {
        for(int i = 0; i < arr.length; i++) 
            System.out.println(i + ": " + arr[i]);
    }

    private static int[] countHash(int[] arr, int max) {
        int[] countArr = new int[max + 1];
        for(int i : arr) 
            countArr[arr[i]]++;
        return countArr;
    }
}
