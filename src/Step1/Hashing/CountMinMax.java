import java.util.*;

public class CountMinMax {
    public static void main(String[] args) {
        int[] arr = { 11, 13, 3, 14, 17, 3, 7, 9, 1, 11, 9, 15, 5, 2, 2, 3 };
        int[] result = getFrequencies(arr);
        printArray(result);
    }

    private static void printArray(int[] arr) {
        for(int i : arr)
            System.out.print(i + " ");
        System.out.println();
    }

    private static int[] getFrequencies(int[] v) {
        Map<Integer, Integer> countMap = new HashMap<>();

        for(int i = 0; i < v.length; i++) {
            if(countMap.containsKey(v[i]))
                countMap.put(v[i], countMap.get(v[i]) + 1);
            else
                countMap.put(v[i], 1);
        }
        System.out.println(countMap);
        int maxFrequency = 0, minFrequency = v.length;
        int maxElement = 0, minElement = 0;

        for(Map.Entry<Integer, Integer> entry : countMap.entrySet()) {
            int element = entry.getKey();
            int count = entry.getValue();

            if(count > maxFrequency) {
                maxFrequency = count;
                maxElement = element;
            }

            if(count < minFrequency) {
                minFrequency = count;
                minElement = element;
            }
        }
        int[] result = {maxElement, minElement}; 
        return result;
    }
}
