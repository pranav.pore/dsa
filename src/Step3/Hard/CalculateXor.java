package Step3.Hard;

import java.util.*;

public class CalculateXor {
    public static void main(String[] args) {
        int[] arr = {4, 2, 2, 6, 4};
        System.out.println(calculateXor(arr, 6));
    }

    private static int calculateXor(int[] arr, int k) {
        int count = 0;
        Map<Integer, Integer> countMap = new HashMap<>();
        countMap.put(0, 1);
        int calculatedXor = 0;
        for(int i : arr) {
            calculatedXor ^= i;
            int remainingXor = calculatedXor ^ k;
            if(countMap.containsKey(remainingXor))
                count += countMap.get(remainingXor);
            countMap.put(calculatedXor, countMap.getOrDefault(calculatedXor, 0) + 1);
        }
        return count;
    }
}
