package Step3.Hard;
import java.util.*;

public class MergeSubArrays {
    public static void main(String[] args) {
        int[][] arr = {{1, 3}, {2, 4}, {3, 5}, {6, 7}};
        System.out.println(Arrays.deepToString(mergeSubArrays(arr)));
    }

    private static int[][] mergeSubArrays(int[][] arr) {
        Arrays.sort(arr, new Comparator<>() {
            @Override
            public int compare(int[] o1, int[] o2) {
                return o1[0] - o2[0];
            }
        });
        List<List<Integer>> resultArr = new ArrayList<>();
        for (int[] ints : arr) {
            if (resultArr.isEmpty() || ints[0] > resultArr.getLast().get(1))
                resultArr.add(Arrays.asList(ints[0], ints[1]));
            else
                resultArr.getLast().set(1, Math.max(resultArr.getLast().get(1), ints[1]));
        }
        return convertToListToArray(resultArr);
    }

    private static int[][] convertToListToArray(List<List<Integer>> list) {
        int numRows = list.size();
        int[][] resultArray = new int[numRows][];

        for (int i = 0; i < numRows; i++) {
            List<Integer> rowList = list.get(i);
            int numCols = rowList.size();
            resultArray[i] = new int[numCols];

            for (int j = 0; j < numCols; j++) {
                resultArray[i][j] = rowList.get(j);
            }
        }

        return resultArray;
    }
}
