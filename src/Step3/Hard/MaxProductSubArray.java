package Step3.Hard;

public class MaxProductSubArray {
    public static void main(String[] args) {
        int[] arr = { -2, 3, -4, 0 };
        System.out.println(subArrayWithMaxProduct(arr));
    }

    private static int subArrayWithMaxProduct(int[] arr) {
        int maxProduct = arr[0];
        for(int i = 0; i < arr.length; i++) {
            int product = arr[i];
            for(int j = i + 1; j < arr.length; j++) {
                maxProduct = Math.max(maxProduct, product);
                product *= arr[j];
            }
            maxProduct = Math.max(maxProduct, product);
        }
        return maxProduct;
    }
}
