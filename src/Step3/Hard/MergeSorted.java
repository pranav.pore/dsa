package Step3.Hard;

public class MergeSorted {
    public static void main(String[] args) {
        int[] arr1 = {1, 2, 3, 0, 0, 0};
        int[] arr2 = {4, 5, 6};
        mergeSorted(arr1, arr2, 3, 3);
        printArray(arr1);
    }

    private static void printArray(int[] arr) {
        for(int i : arr)
            System.out.print(i + " ");
        System.out.println();
    }

    private static void mergeSorted(int[] arr1, int[] arr2, int m, int n) {
        int i = m - 1;
        int j = n - 1;
        int end = m + n - 1;

        while (i >= 0 && j >= 0) {
            if(arr1[i] > arr2[j]) {
                arr1[end] = arr1[i];
                i--;
            }
            else {
                arr1[end] = arr2[j];
                j--;
            }
            end--;
        }

        while(j >= 0) {
            arr1[end] = arr2[j];
            end--;
            j--;
        }
    }
}
