package Step3.Hard;
import java.util.*;

public class N3Majority {
    public static void main(String[] args) {
        int[] arr = { 3,2,3 };
        System.out.println(n3Majority(arr));
        System.out.println(n3MajorityOptimal(arr));
    }

    private static ArrayList<Integer> n3Majority(int[] arr) {
        Map<Integer, Integer> countMap = new HashMap<>();
        ArrayList<Integer> resultArr = new ArrayList<>();
        int majority = (arr.length / 3) + 1;
        for(int i : arr) {
            if(countMap.containsKey(i))
                countMap.put(i, countMap.get(i) + 1);
            else    
                countMap.put(i, 1);
            if(countMap.get(i) == majority)
                resultArr.add(i);
            if(resultArr.size() == 2)
                break;
        }
        
        return resultArr;
    }

    private static List<Integer> n3MajorityOptimal(int[] arr) {
        int el1 = arr[0], el2 = arr[0];
        int count1 = 0, count2 = 0;
        for(int i : arr) {
            if(count1 == 0 && i != el2) {
                count1 = 1;
                el1 = i;
            }
            else if(count2 == 0 && i != el1) {
                count2 = 1;
                el2 = i;
            }
            else if(i == el1) count1++;
            else if(i == el2) count2++;
            else {count1--; count2--;}
        }
        List<Integer> resultArr = new ArrayList<>();
        int count1Verify = 0;
        for(int i : arr) {
            if(i == el1) 
                count1Verify++;
        }
        if(count1Verify > arr.length / 3) resultArr.add(el1);
        
        int count2Verify = 0;
        for(int i : arr) {
            if(i == el2) 
                count2Verify++;
        }
        if(count2Verify > arr.length / 3 && !resultArr.contains(el2)) resultArr.add(el2);

        return resultArr;

    }
}
