package Step3.Hard;

import java.util.ArrayList;
import java.util.Arrays;

public class ReversePairs {
    public static void main(String[] args) {
        int[] arr = {1,3,2,3,1};
        System.out.println(reversePairsOptimal(arr));
    }

    private static int reversePairs(int[] arr) {
        int count = 0;
        for(int i = 0; i < arr.length; i++) {
            for(int j = i + 1; j < arr.length; j++) {
                if(arr[i] > 2 * arr[j])
                    count++;
            }
        }
        return count;
    }

    private static int reversePairsOptimal(int[] arr) {
        return mergeSort(arr, 0, arr.length - 1);
    }

    private static int mergeSort(int[] arr, int left, int right) {
        int count = 0;
        if (left < right) {
            int mid = left + (right - left) / 2;

            count += mergeSort(arr, left, mid);
            count += mergeSort(arr, mid + 1, right);
            count += countArrays(arr, left, mid, right);
            merge(arr, left, mid, right);
        }
        return count;
    }

    private static void merge(int[] arr, int left, int mid, int right) {
        int i = 0;
        int j = mid + 1;
        ArrayList<Integer> mergedArray = new ArrayList<>();
        insertElements(arr, left, mid, right, i, j, mergedArray);
    }

    public static void insertElements(int[] arr, int left, int mid, int right, int i, int j, ArrayList<Integer> mergedArray) {
        while (i <= mid && j <= right) {
            if (arr[i] <= arr[j]) {
                mergedArray.add(arr[i]);
                i++;
            } else {
                mergedArray.add(arr[j]);
                j++;
            }
        }
        while (i <= mid) {
            mergedArray.add(arr[i]);
            i++;
        }
        while (j <= right) {
            mergedArray.add(arr[j]);
            j++;
        }
        for (int k = left; k <= right; k++) {
            arr[k] = mergedArray.get(k - left);
        }
    }

    private static int countArrays(int[] arr, int low, int mid, int high) {
        int right = mid + 1;
        int count = 0;
        for (int i = low; i <= mid; i++) {
            while (right <= high && (long) arr[i] > 2 * (long) arr[right]) right++;
            count += (right - mid - 1);
        }
        return count;
    }
}
