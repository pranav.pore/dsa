package Step3.Hard;
import java.util.*;

public class PascalsTriangle {
    public static void main(String[] args) {
        int numRows = 5;
        printList(pascalsTriangleOptimal(numRows));
    }

    private static void printList(List<List<Integer>> listOfLists) {
        System.out.print("[");
        for (List<Integer> innerList : listOfLists) {
            System.out.print("[");
            for (int i = 0; i < innerList.size(); i++) {
                System.out.print(innerList.get(i));
                if (i < innerList.size() - 1) {
                    System.out.print(", ");
                }
            }
            System.out.print("]");
        }
        System.out.println("]");
    }

    private static List<List<Integer>> pascalsTriangle(int numRows) {
        List<List<Integer>> outerList = new ArrayList<>();
        if(numRows == 0) return outerList;

        List<Integer> firstRow = new ArrayList<>();
        firstRow.add(1);
        outerList.add(firstRow);

        for(int i = 1; i < numRows; i++) {
            List<Integer> previousRow = outerList.get(i - 1);
            List<Integer> currentRow = new ArrayList<>();
            currentRow.add(1);
            for(int j = 1; j < i; j++) {
                currentRow.add(previousRow.get(j - 1) + previousRow.get(j));
            }
            currentRow.add(1);
            outerList.add(currentRow);
        }
        return outerList;
    }

    private static List<List<Integer>> pascalsTriangleOptimal(int numRows) {
        List<List<Integer>> outerList = new ArrayList<>();
        for(int i = 1; i <= numRows; i++) {
            outerList.add(generateRow(i));
        }
        return outerList;
    }

    private static List<Integer> generateRow(int rowNum) {
        List<Integer> row = new ArrayList<>();
        int el = 1;
        row.add(1);
        for(int i = 1; i < rowNum; i++) {
            el *= (rowNum - i);
            el /= i;
            row.add(el);
        }
        return row;
    }
}
