package Step3.Hard;

public class MissingRepeating {
    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 2};
        int[] result = missingRepeating(arr);
        printArray(result);
    }

    private static void printArray(int[] arr) {
        for(int i : arr)
            System.out.print(i + " ");
        System.out.println();
    }

    private static int[] missingRepeating(int[] arr) {
        int n = arr.length;
        int s = n * (n + 1) / 2;
        int s2 = n * (n + 1) * (2 * n + 1) / 6;
        int sn = 0, s2n = 0;

        for(int i : arr) {
            sn += i;
            s2n += i * i;
        }

        int value1 = s - sn;
        int value2 = (s2 - s2n) / value1;
        return new int[]{(value1 + value2) / 2, ((value2 - value1) / 2)};
    }
}
