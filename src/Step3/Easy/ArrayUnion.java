package Step3.Easy;
import java.util.*;

public class ArrayUnion {
    public static void main(String[] args) {
        int[] n = { 1, 2, 3, 3 };
        int[] m = { 2, 2, 4, 5, 6, 8 };
        System.out.println(findUnion(n, m).toString());
    }    

    private static ArrayList<Integer> findUnion(int[] a, int[] b) {
        int aLength = a.length;
        int bLength = b.length;
        int i = 0;
        int j = 0;
        ArrayList<Integer> resultArr = new ArrayList<>();
        while(i < aLength && j < bLength) {
            if(a[i] < b[j]) {
                if(resultArr.size() == 0 || resultArr.getLast() != a[i])
                    resultArr.add(a[i]);
                i++;
            } 
            else {
                if(resultArr.size() == 0 || resultArr.getLast() != b[j])
                    resultArr.add(b[j]);
                j++;
            }
        }
        while(i < aLength) {
            if(resultArr.size() == 0 || resultArr.getLast() != a[i])
                resultArr.add(a[i]);
            i++;
        }
        while(j < bLength) {
            if(resultArr.size() == 0 || resultArr.getLast() != b[j])
                resultArr.add(b[j]);
            j++;
        }
        return resultArr;
    }
}
