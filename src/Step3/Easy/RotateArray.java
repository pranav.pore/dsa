package Step3.Easy;

public class RotateArray {
    public static void main(String[] args) {
        int[] arr = { 1, 2, 3, 4, 5, 6, 7 };
        int k = 3;
        rotateArray(arr, k);
        printArray(arr);
    }

    private static void printArray(int[] arr) {
        for(int i : arr)
            System.out.print(i + " ");
        System.out.println();
    }   

    private static void rotateArray(int[] arr, int k) {   
        int[] resultArr = new int[arr.length];
        for(int i = 0; i < arr.length; i++) {
            resultArr[(i + k) % arr.length] = arr[i];
        }
        for(int i = 0; i < arr.length; i++)
            arr[i] = resultArr[i];
    }
}
