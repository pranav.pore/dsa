package Step3.Easy;
import java.util.*;

public class SubArraySum {
    public static void main(String[] args) {
        int[] arr = { 1, 2, 3, 1, 1, 1, 1 };
        System.out.println(subArraySum(arr, 3));
    }

    private static int subArraySum(int[] arr, int k) {
        int left = 0; 
        int right = 0;
        long sum = arr[0];
        int maxLength = 0;
        int n = arr.length;
        while(right < n) {
            while(left <= right && sum > k) {
                sum -= arr[left];
                left++;
            }
            if(sum == k) 
                maxLength = Math.max(maxLength, right - left + 1);
            right++;
            if(right < n) sum += arr[right];
        }
        return maxLength;
    }    
}
