package Step3.Easy;
import java.util.*;

public class ArrayIntersection {
    public static void main(String[] args) {
        int[] n = { 1, 2, 3, 4 };
        int[] m = { 1, 2, 3, 4 };
        System.out.println(findIntersection(n, m).toString());
    }    

    private static ArrayList<Integer> findIntersection(int[] a, int[] b) {
        int aLength = a.length;
        int bLength = b.length;
        int i = 0;
        int j = 0;
        Set<Integer> resultSet = new HashSet<>();
        while(i < aLength && j < bLength) {
            if(a[i] < b[j]) i++;
            else if(a[i] > b[j]) j++;
            else {
                resultSet.add(a[i]);
                i++;
                j++;
            }
        }
        ArrayList<Integer> resultArr = new ArrayList<>(resultSet);
        return resultArr;
    }
}
