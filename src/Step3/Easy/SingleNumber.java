package Step3.Easy;
import java.util.*;

public class SingleNumber {
    public static void main(String[] args) {
        int[] arr = { 4, 1, 2, 1, 2 };
        System.out.println(singleNumber(arr));
    }

    private static int singleNumber(int[] arr) {
        if(arr.length == 1) return arr[0];
        Arrays.sort(arr);
        int i = 0;
        while(i < arr.length - 1) {
            if(arr[i] != arr[i + 1])
                return arr[i];
            else
                i += 2;
        }
        return arr[arr.length - 1];
    }
}
