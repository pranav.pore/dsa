package Step3.Easy;

public class RemoveDuplicates {
    public static void main(String[] args) {
        int[] arr = { 0, 0, 1, 1, 1, 2, 2, 3, 3, 4 };
        removeDuplicates(arr);
        printArray(arr);
    }

    private static void printArray(int[] arr) {
        for(int i : arr)
            System.out.print(i + " ");
        System.out.println();
    }    

    private static void removeDuplicates(int[] arr) {
        int lastFilledIndex = 0;
        int lastFilledNum = arr[0];

        for(int i = 1; i < arr.length; i++) {
            if(arr[i] != lastFilledNum) {
                arr[++lastFilledIndex] = arr[i];
                lastFilledNum = arr[i];
            }
        }
    }
}
