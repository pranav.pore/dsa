package Step3.Easy;

public class ConsecutiveOnes {
    public static void main(String[] args) {
        int[] arr = { 1, 1, 0, 1, 1, 1 };
        System.out.println(consecutiveOnes(arr));
    }

    private static int consecutiveOnes(int[] arr) {
        int currentCount = 0;
        int maxCount = 0;
        for(int i = 0; i < arr.length; i++) {
            while(i < arr.length && arr[i] == 1) {
                currentCount++;
                i++;
            }
            if(currentCount > maxCount)
                maxCount = currentCount;
            currentCount = 0;
            
        }
        return maxCount;
    }
}
