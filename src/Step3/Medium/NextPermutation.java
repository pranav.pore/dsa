package Step3.Medium;
import java.util.*;

public class NextPermutation {
    public static void main(String[] args) {
        int[] nums = { 1, 1, 5 };
        nextPermutation(nums);
        printArray(nums);
    }

    private static void printArray(int[] nums) {
        for(int i : nums) 
            System.out.print(i + " ");
        System.out.println();
    }

    private static void reverse(int[] arr, int start, int end) {
        while(start < end) {
            int temp = arr[start];
            arr[start] = arr[end];
            arr[end] = temp;
            start++;
            end--;
        }
    }

    private static void nextPermutation(int[] nums) {
        int n = nums.length;
        int breakPoint = -1;

        for(int i = n - 2; i >= 0; i--) {
            if(nums[i] < nums[i + 1]) {
                breakPoint = i;
                break;
            }
        }

        if(breakPoint == -1) {
            reverse(nums, 0, n - 1);
            return;
        }

        for(int i = n - 1; i > breakPoint; i--) {
            if(nums[i] > nums[breakPoint]) {
                int temp = nums[i];
                nums[i] = nums[breakPoint];
                nums[breakPoint] = temp;
                break;
            }
        }

        reverse(nums, breakPoint + 1, n - 1);
    }
}
