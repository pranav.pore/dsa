package Step3.Medium;
import java.util.*;

public class MajorityElement {
    public static void main(String[] args) {
        int[] arr = { 6, 5, 5 };
        System.out.println(majorityElement(arr));
        System.out.println(majorityElementMoores(arr));
    }

    private static int majorityElement(int[] arr) {
        Map<Integer, Integer> hashMap = new HashMap<>();
        for(int i = 0; i < arr.length; i++) {
            if(hashMap.containsKey(arr[i]))
                hashMap.put(arr[i], hashMap.get(arr[i]) + 1);
            else
                hashMap.put(arr[i], 1);
        }
        for(Map.Entry<Integer, Integer> entry : hashMap.entrySet()) {
            if(entry.getValue() > arr.length / 2)
                return entry.getKey();
        }
        return -1;
    }

    private static int majorityElementMoores(int[] arr) {
        int count = 0;
        int element = arr[0];
        for(int i = 0; i < arr.length; i++) {
            if(count == 0) {
                count = 1;
                element = arr[i];
            }
            else if(arr[i] == element) 
                count++;
            else 
                count--;
        }

        int count1 = 0;
        for(int i = 0; i < arr.length; i++) {
            if(arr[i] == element)
                count1++;
        }
        if(count1 > arr.length / 2)
            return element;
        return -1;
    }
}
