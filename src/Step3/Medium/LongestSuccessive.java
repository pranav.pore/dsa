package Step3.Medium;

import java.util.*;

public class LongestSuccessive {
    public static void main(String[] args) {
        int[] arr = { 5, 8, 3, 2, 1, 4 };
        System.out.println(longestSuccessive(arr));
    }   
    
    private static int longestSuccessive(int[] nums) {
        Arrays.sort(nums);
        int n = nums.length;
        int lastSmaller = Integer.MIN_VALUE;
        int count = 0;
        int longest = 1;

        for(int i = 0; i < n; i++) {
            if(nums[i] == lastSmaller + 1) {
                count++;
                lastSmaller = nums[i];
            } else if(nums[i] != lastSmaller) {
                count = 1;
                lastSmaller = nums[i];
            }
            longest = Math.max(longest, count);
        }

        return longest;
    }

    private static int longestSuccessiveOptimal(int[] arr) {
        if(arr.length == 0) return 0;
        Set<Integer> arrSet = new HashSet<Integer>();
        int maxCount = 1;
        for(int i : arr)
            arrSet.add(i);
        for(int i : arrSet) {
            if(!arrSet.contains(i - 1)) {
                int count = 1;
                int tempI = i;
                while(arrSet.contains(tempI + 1)) {
                    tempI++;
                    count++;
                }
                maxCount = Math.max(maxCount, count);
            }
        }
        return maxCount;
    }
}
