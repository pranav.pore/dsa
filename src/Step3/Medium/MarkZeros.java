package Step3.Medium;

public class MarkZeros {
    public static void main(String[] args) {
        Integer[][] matrix = {{0,1,2,0}, {3, 4, 5, 2}, {1, 3, 1, 5}};
        //setZerosBrute(matrix);
        //setZeros(matrix);
        setZerosOptimal(matrix);
        printMatrix(matrix);
    }    

    private static <T> void printMatrix(T[][] matrix) {
        for(int i = 0; i < matrix.length; i++) {
            for(int j = 0; j < matrix[0].length; j++)
                System.out.print(matrix[i][j] + " ");
            System.out.println();
        }
    }

    private static void setZerosBrute(Integer[][] matrix) {
        Boolean[][] flagMatrix = new Boolean[matrix.length][matrix[0].length];
        for (int i = 0; i < flagMatrix.length; i++) {
            for (int j = 0; j < flagMatrix[0].length; j++) {
                flagMatrix[i][j] = Boolean.FALSE;
            }
        }
        for(int i = 0; i < matrix.length; i++) {
            for(int j = 0; j < matrix[0].length; j++) {
                if(matrix[i][j] == 0)
                    flagMatrix[i][j] = true;
            }
        }
        for(int i = 0; i < matrix.length; i++) {
            for(int j = 0; j < matrix[0].length; j++) {
                if(matrix[i][j] == 0 && flagMatrix[i][j]) {
                    for(int k = 0; k < matrix[0].length; k++)
                        matrix[i][k] = 0;
                    for(int k = 0; k < matrix.length; k++)
                        matrix[k][j] = 0;
                }
            }
        }
        printMatrix(flagMatrix);
    }

    private static void setZeros(Integer[][] matrix) {
        int[] rows = new int[matrix.length];
        int[] cols = new int[matrix[0].length];
        for(int i = 0; i < matrix.length; i++) {
            for(int j = 0; j < matrix[0].length; j++) {
                if(matrix[i][j] == 0) {
                    rows[i] = 1;
                    cols[j] = 1;
                }
            }
        }
        for(int i = 0; i < matrix.length; i++) {
            for(int j = 0; j < matrix[0].length; j++) {
                if(rows[i] == 1 || cols[j] == 1) {
                    matrix[i][j] = 0;
                }
            }
        }
    }

    private static void setZerosOptimal(Integer[][] matrix) {
        int col0 = 1;
        for(int i = 0; i < matrix.length; i++) {
            for(int j = 0; j < matrix[0].length; j++) {
                if(matrix[i][j] == 0) {
                    matrix[i][0] = 0; //1st Column
                    if(j != 0)
                        matrix[0][j] = 0; //1st Row
                    else
                        col0 = 0;
                }
                
            }
        }
        for(int i = 1; i < matrix.length; i++) {
            for(int j = 1; j < matrix[0].length; j++) {
                if(matrix[i][j] != 0) {
                    if(matrix[0][j] == 0 || matrix[i][0] == 0) {
                        matrix[i][j] = 0;
                    }
                }
                
            }
        }
        for(int j = 0; j < matrix[0].length; j++) {
            if(matrix[0][0] == 0)
                matrix[0][j] = 0;
        }
        if(col0 == 0) {
            for(int i = 0; i < matrix.length; i++) 
                matrix[i][0] = 0;
        }
        
    }
}
