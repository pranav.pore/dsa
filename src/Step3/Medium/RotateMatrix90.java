package Step3.Medium;

public class RotateMatrix90 {
    public static void main(String[] args) {
        // int[][] matrix = {{ 5, 1, 9, 11 }, { 2, 4, 8, 10 }, { 13, 3, 6, 7 }, { 15, 14, 12, 16 }};
        // int[][] resultMatrix = rotateMatrix(matrix);
        int[][] matrix = {{ 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 }};
        rotateMatrixSwap(matrix);
        printMatrix(matrix);
    }

    private static void printMatrix(int[][] matrix) {
        for(int i = 0; i < matrix.length; i++) {
            for(int j = 0; j < matrix[0].length; j++)
                System.out.print(matrix[i][j] + " ");
            System.out.println();
        }
    }

    private static int[][] rotateMatrix(int[][] matrix) {
        int[][] resultMatrix = new int[matrix[0].length][matrix.length];
        for(int i = 0; i < matrix.length; i++) {
            for(int j = 0; j < matrix[0].length; j++) {
                resultMatrix[j][matrix.length - (i + 1)] = matrix[i][j];
            }
        }
        return resultMatrix;
    }

    private static void rotateMatrixSwap(int[][] matrix) {
        transposeMatrix(matrix);
        for(int i = 0; i < matrix.length; i++) {
            int left = 0;
            int right = matrix[0].length - 1;
            while(left <= right) {
                int temp = matrix[i][left];
                matrix[i][left] = matrix[i][right];
                matrix[i][right] = temp;
                left++;
                right--;
            }
        }
    }

    private static void transposeMatrix(int[][] matrix) {
        for(int i = 0; i < matrix.length; i++) {
            for(int j = i + 1; j < matrix[0].length; j++) {
                int temp = matrix[i][j];
                matrix[i][j] = matrix[j][i];
                matrix[j][i] = temp;
            }
        }
    }
}
