package Step3.Medium;

import java.util.ArrayList;

public class AlternatePosNeg {
    public static void main(String[] args) {
        int[] arr = {3, 1, -2, -5, 2, -4};
        int[] resultArr = alternatePosNeg(arr);
        printArray(resultArr);
    }   

    private static void printArray(int[] arr) {
        for(int i : arr) 
            System.out.print(i + " ");
        System.out.println();
    }
    
    private static int[] alternatePosNeg(int[] arr) {
        int[] resultArr = new int[arr.length];
        int posIdx = 0;
        int negIdx = 1;

        for(int i = 0; i < arr.length; i++) {
            if(arr[i] > 0) {
                resultArr[posIdx] = arr[i];
                posIdx += 2;
            } else {
                resultArr[negIdx] = arr[i];
                negIdx += 2;
            }
        }

        return resultArr;
    }
}
