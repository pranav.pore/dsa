package Step3.Medium;
import java.util.*;

public class MaxSubArrSum {
    public static void main(String[] args) {
        int[] arr= { 1, 2, 3, 1, 1, 1, 1 };
        System.out.println(maxSubArrSum(arr, 3));
    }

    private static int maxSubArrSum(int[] arr, int k) {
        int sum = 0;
        HashMap<Integer, Integer> preSumMap = new HashMap<>();
        int maxLength = 0;
        for(int i = 0; i < arr.length; i++) {
            sum += arr[i];
            int rem = sum - k;
            if(preSumMap.containsKey(rem)) {
                maxLength = Math.max(maxLength, i - preSumMap.get(rem));
            }
            if(!preSumMap.containsKey(sum)) 
                preSumMap.put(sum, i);
        }

        return maxLength;
    }
}
