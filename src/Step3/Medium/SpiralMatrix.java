package Step3.Medium;
import java.util.ArrayList;

public class SpiralMatrix {
    public static void main(String[] args) {
        int[][] arr = {{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 10, 11, 12}};
        System.out.println(printSpiral(arr).toString());
    }

    private static ArrayList<Integer> printSpiral(int[][] arr) {
        int sCol = 0;
        int eCol = arr[0].length - 1;
        int sRow = 0;
        int eRow = arr.length - 1;
        int count = 0;
        int total = arr[0].length * arr.length;
        ArrayList<Integer> resultArr = new ArrayList<>();
        while(count < arr[0].length * arr.length) {
            for(int i = sCol; i <= eCol && count < total; i++) {
                resultArr.add(arr[sRow][i]);
                count++;
            }
            sRow++;
            for(int i = sRow; i <= eRow && count < total; i++) {
                resultArr.add(arr[i][eCol]);
                count++;
            }
            eCol--;
            for(int i = eCol; i >= sCol && count < total; i--) {
                resultArr.add(arr[eRow][i]);
                count++;
            }
            eRow--;
            for(int i = eRow; i >= sRow && count < total; i--) {
                resultArr.add(arr[i][sCol]);
                count++;
            }
            sCol++;
        }

        return resultArr;
    }
}
