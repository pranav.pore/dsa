package Step3.Medium;

public class StockBuySell {
    public static void main(String[] args) {
        int[] arr= { 7, 1, 5, 3, 6, 4 };
        System.out.println(stockBuySell(arr));
    }   
    
    private static int stockBuySell(int[] arr) {
        int currentLeast = Integer.MAX_VALUE;
        int currentProfit = 0;
        int maxProfit = 0;

        for(int i = 0; i < arr.length; i++) {
            if(arr[i] < currentLeast) 
                currentLeast = arr[i];
            currentProfit = arr[i] - currentLeast;
            if(currentProfit > maxProfit) 
                maxProfit = currentProfit;
        }

        return maxProfit;
    }
}
