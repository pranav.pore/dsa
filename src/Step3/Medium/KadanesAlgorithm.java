package Step3.Medium;

public class KadanesAlgorithm {
    public static void main(String[] args) {
        int[] arr= { 7, 1, 5, 3, 6, 4 };
        System.out.println(kadanesAlgorithm(arr));
    };

    private static int kadanesAlgorithm(int[] arr) {
        int sum = 0;
        int maxSum = arr[0];

        for(int i : arr) {
            sum += i;
            maxSum = Math.max(maxSum, sum);
            if(sum < 0) sum = 0;
        }

        return maxSum;
    }
}