package Step3.Medium;

public class KadanesAlgorithmArr {
    public static void main(String[] args) {
        int[] arr= { -2, 1, -3, 4, -1, 2, 1, -5, 4 };
        int[] result = kadanesAlgorithm(arr);
    };
    
    private static int[] kadanesAlgorithm(int[] arr) {
        int sum = 0;
        int maxSum = 0;
        int left = 0;
        int right = 0;
        for(int i = 0; i < arr.length; i++) {
            sum += arr[i];
            if(sum > maxSum) {
                maxSum = sum;
                right = i;
            }
            if(sum < 0){
                left = i + 1;
                sum = 0;
            }
        }
        int[] resultArr = new int[right - left + 1];
        for(int i = left; i <= right; i++)
            resultArr[i - left] = arr[i];
        return resultArr;
    }
}
