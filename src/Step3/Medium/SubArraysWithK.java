package Step3.Medium;
import java.util.*;

public class SubArraysWithK {
    public static void main(String[] args) {
        int[] arr = { 1, 2, 3 };
        int k = 3;
        System.out.println(subArraysWithKOptimal(arr, k));
    }

    private static int subArraysWithK(int[] arr, int k) {
        int count = 0;
        for(int i = 0; i < arr.length; i++) {
            int sum = 0;
            for(int j = i; j < arr.length; j++) {
                sum += arr[j];
                if(sum == k)
                    count++;
            }
        }

        return count;
    }

    private static int subArraysWithKOptimal(int[] arr, int k) {
        int count = 0;
        Map<Integer, Integer> preSumMap = new HashMap<>();
        int sum = 0;
        for(int i = 0; i < arr.length; i++) {
            sum += arr[i];
            if(sum == k) count++;
            int remainingSum = sum - k;
            if(preSumMap.containsKey(remainingSum)) {
                count += preSumMap.get(remainingSum);
            }
            preSumMap.put(sum, preSumMap.getOrDefault(sum, 0) + 1);
        }
        return count;
    }
}
