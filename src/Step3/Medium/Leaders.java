package Step3.Medium;

import java.util.ArrayList;

public class Leaders {
    public static void main(String[] args) {
        int[] arr = { 1, 2, 3, 2 };
        int[] leadersArr = leaders(arr);
        printArray(leadersArr); 
    }

    private static void printArray(int[] nums) {
        for(int i : nums) 
            System.out.print(i + " ");
        System.out.println();
    }

    private static int[] leaders(int[] arr) {
        ArrayList<Integer> leadersList = new ArrayList<>();
        leadersList.add(arr[arr.length - 1]);
        int backMax = arr[arr.length - 1];
        
        for(int i = arr.length - 2; i >= 0; i--) {
            if(arr[i] > backMax) {
                leadersList.add(arr[i]);
                backMax = arr[i];
            }
        }

        int[] result = new int[leadersList.size()];
        for(int i = 0; i < leadersList.size(); i++)
            result[i] = leadersList.get(i);
        
        return result;
    }
    
}
