package Step3.Medium;
import java.util.*;

public class TwoSum {
    public static void main(String[] args) {
        int[] arr = {3,2,4};
        int target = 6;
        int[] result = twoSumOptimized(arr, target);
        printArray(result);
    }    

    private static void printArray(int[] result) {
        for(int i : result)
            System.out.print(i + " ");
        System.out.println();
    }

    private static int[] twoSum(int[] arr, int target) {
        for(int i = 0; i < arr.length; i++) {
            for(int j = i + 1; j < arr.length; j++) {
                if(arr[i] + arr[j] == target) 
                    return new int[]{i, j};
            }
        }
        return new int[]{-1, -1};
    }

    private static int[] twoSumOptimized(int[] arr, int target) {
        Map<Integer, Integer> requiredHashMap = new HashMap<>();
        for(int i = 0; i < arr.length; i++) {
            int requiredMore = target - arr[i];
            if(requiredHashMap.containsKey(requiredMore)) 
                return new int[]{requiredHashMap.get(requiredMore), i};
            else 
                requiredHashMap.put(arr[i], i);
        } 
        return new int[]{-1, -1};
    }
}
